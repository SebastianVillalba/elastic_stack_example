# Elastic Stack with Docker

## How to run the Stack

To initialize all the needed services.

```
docker-compose up -d 
```  

## How to run PHP sample app

Install needed PHP dependencies

```
cd app && composer install
```  

App en PHP con ejemplo de uso de monoLog y consumo en Elasticsearch enviado por fileBeat
