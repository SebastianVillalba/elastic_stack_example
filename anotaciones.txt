elastic
	- Se crea un cluster
	- casi realtime: Hay procesos que se actualizan diariamente para su búsqueda. Elastic search es casi real time. Se ingestan los datos y ya están disponibles

Elastic search
	- es Schemaless
	- DOcumento: un json = fila de una tabla. Usa Dynamic Mapping para determinar el esquema automático del documento.
	- Indices: representan la DB en una relacional. Es donde se guardan los documentos.
	- DSL (Domain Specific Language): Es un lenguaje para realizar querys en elasticsearch
	 http://localhost:9200/chefsms/_search?q=level:ERROR
	- Monitoreo reactivo, se pueden crear patrones para que avise si el comportamiento es diferentes. (cantidad de 400 en equis tiempo), etc.
	- Se puede relacionar con slack
	- Definir un indice por día (facilita cuando se quiera eliminar logs)
	- Ejemplos de visualizaciones:
		- ver carga de trabajo de servidores
		- cual tiene mas 404
		- agrupar respuestas de http
		- login por paises
		- processUniqueId => para hacer un seguimiento de un proceso
	- Logging docker driver (para habilitar el log en docker) (escribir en standard output)
	
	 
NOTAS
	- El mensaje tiene que ser siempre general, y las variables van en el contexto: $log->warn("No se pudo conectar a la DB", ['DB'=>'100.0.0.101']);
	

	
	