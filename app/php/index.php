<?php

require __DIR__ . '/vendor/autoload.php';

use Monolog\Logger;
use Elastica\Client;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\ElasticSearchHandler;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\RotatingFileHandler;

// create a log channel
$log = new Logger('logger');

//Log to stdout
$stdoutHandler = new ErrorLogHandler();
$formatter = new JsonFormatter();
$stdoutHandler->setFormatter($formatter);
$log->pushHandler($stdoutHandler);

// File Handler
$fileHandler = new RotatingFileHandler('/var/www/html/logs/app.log', 0, Logger::DEBUG);
$formatter = new JsonFormatter();
$fileHandler->setFormatter($formatter);
$log->pushHandler($fileHandler);

// Elasticsearch Handler
// $elasticaClient = new Client(
    // [
        // 'host' => 'localhost',
        // 'port' => 9200
    // ]
// );

// $elasticsearchHandler = new ElasticSearchHandler($elasticaClient);
// $log->pushHandler($elasticsearchHandler);

// El mensaje tiene que ser general. No agregar info adicional. La info adicional se agregan en el contexto, ya que terminan siendo Keys y lo vamos a usar como busqueda

$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 1, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312323']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 3, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312324']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Teleprom', 'Instancia' => 5, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312322']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 2, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312313']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'NRS', 'Instancia' => 1, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312313']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 3, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312345']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 3, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312356']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'NRS', 'Instancia' => 4, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312378']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Inalambria', 'Instancia' => 4, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312353']);
$log->info('No se pudo enviar el SMS', ['Gateway' => 'Teleprom', 'Instancia' => 4, 'Mensaje' => 'SMS de prueba', 'Telefono' => '1542312342']);


// // My Application
// $options = getopt('a:b:');

// # App Servidor A
// if ($options['a'] === 'warning') {
    // $log->warn('Esto es un Warning', ['Servidor' => 'Servidor A']);
// } else {
    // $log->info('Esto es un Info', ['Servidor' => 'Servidor C']);
// }

// # App Servidor B
// if ($options['b'] === 'error') {
    // $log->error('Esto es un Error', ['Servidor' => 'Servidor B']);
// } else {
    // $log->info('Esto es un Info', ['Servidor' => 'Servidor D']);
// }

